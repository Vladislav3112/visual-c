#include <iostream>
#include"vector.h"
using namespace std;

ostream& operator<<(ostream& out, const vector& vec) {//���������� ���������, ���������� ������ 
                                                      // const methods, �.� vec - const
	for (int i = 0; i < vec.getSize(); i++)
		out << vec.getElemAt(i) << " ";
	out << endl;
	return out;
}

int main()
{
	int* arr = new int[10];
	for (int i = 0; i < 10; i++)
		arr[i] = i;

	vector vec(arr,10);
	vec.print();
	
	/*void testLifecycle()
	{
		cout << "function starts" << endl;
		vector vec;
		cout << "function ends" << endl;
	}*/
	vector* vecPtr;//no constructor
	vector*vec_3 = new vector(arr, 5);
	vec_3->push_back(1);			//������������ �������� �������
	//((int*)vec_3)[0] = (int)&vec_3;//������ � ��������� �����, ������ �������
	((int*)((int*)vec_3)[0])[0] = 3;//��������� ������� �������� �������
	/*vec.data = new int[10];
	for (int i = 0; i < 10; i++)
		vec.data[i] = i;
	vec.size = 10;
	vec.print();*/

	vec_3->print();
	cout << *vec_3 << endl;
	delete[] arr;
	delete vec_3;
	cout << "before function starts" << endl;
	//testLifecycle();
	cout << "after function ends" << endl;

	vector vec2(10);
	cout << "vec actual size:" << vec2.getSize() << endl;
	for (int i = 0; i < 10; i++)
		vec2.push_back(i);
	cout << "vec actual size:" << vec2.getSize() << endl;
	for (int i = 0; i < vec.getSize(); i++)
		cout<<vec2.getElemAt(i)<<" ";
	cout << endl;
	int size = vec2.getSize();

	int& thirdElem = vec2.getElemAt(3);
	thirdElem = 100;
	cout << vec2.getElemAt(3) << endl;
	cout << "vec actual size:" << vec2.getSize() << endl;
	for (int i = 0; i < size; i++)
		cout << vec2.pop_back() << " ";
    cout << endl;
	cout << vec2.getSize();
	
	vector vecToCopyTo = vec; //����� ������������ �����������
	cout << "\n";
	cout << vector::getObjCount() << endl;
	
	system("pause");
	return 0;
}