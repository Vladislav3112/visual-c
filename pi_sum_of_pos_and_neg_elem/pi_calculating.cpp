#include <iostream>
using namespace std;

int main() {
	double pi=0.0;
	double elem = 1000.0;
	double difference_of_elem;
	int count = 1;
	cout << "Enter d" << endl;
	cin >> difference_of_elem;

	while (difference_of_elem < abs(elem)) {
		if (count % 2 != 0 ) elem = 1.0 / (2*count-1);
		else elem = -1.0 / (2 * count - 1);
		pi += elem;
		count++;
	}
	cout << "pi=" << 4.0*pi;
	system("pause");
	return 0;
}