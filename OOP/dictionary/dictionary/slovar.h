#pragma once
#include "stroka.h"
#include "avl_tree.h"
#include "Pair.h"
class slovar {

	public:
		void add(stroka word, stroka translate) {
			Pair p(word, translate);
			tree.insert(p);

			numberOfWords++;
		}
		stroka operator[](stroka eng)const {
			return (*tree.find(Pair(eng, ""))).getTranslate();
		}
		int getNumberOfWords(){ return numberOfWords; }
	private: 
	int numberOfWords;
	AvlTree<Pair> tree;
};