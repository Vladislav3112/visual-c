#include <iostream>
#include <fstream>
#include "Base64.h"
using namespace std;

int main()
{
	setlocale(LC_ALL, "Ru");
	//������� ����

	ifstream in;
	in.open("in1.txt", ios::binary);
	if (!in.is_open())
	{
		cout << "Input file error!" << endl;
		system("pause");
		return 0;
	}
	//������ ������ �����
	in.seekg(0, ios::end);  //�������� ���������
	int fSize = in.tellg(); //������ �������� ��������� ���������

	cout << "������ ��������� ���������: ";
	cout << fSize << endl << endl;

	//������������ � ������ �����
	in.seekg(0, ios::beg);

	//��������� ��� � ������
	char* fileContent = new char[fSize];
	in.read(fileContent, fSize);

	//�������� ������ � BASE64
	char* encoded = base64_encode(fileContent,fSize);
	int size = strlen(encoded);

	cout << "������ ��������������� ���������: ";
	cout << strlen(encoded) << endl << endl;

	//�������� ����
	ofstream out;
	out.open("out.txt");
	if (!out.is_open())
	{
		cout << "Output file error!" << endl;
		system("pause");
		return 0;
	}
	out << encoded;

	//��������� �����
	out.close();
	in.close();

	//������� ������
	delete[] fileContent;
	delete[] encoded;	

	system("pause");
	return 0;
}