#include <iostream>
using namespace std;

int main() {
	unsigned long long a = 1;
	int k = 0;
	for (int i = 100000; i < 1000000; i++) {
		if (k == 2) break;
		a = i * i;
		if ((a - i) % 1000000 == 0) {
			k++;
			cout << i << endl;
		}
	}
	system("pause");
	return 0;
}