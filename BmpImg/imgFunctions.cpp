#include "ImgFunctions.h"

int ReadRgbImage(char filename[], RgbImg& img)
{
	{
		ifstream bmpIn;
		bmpIn.open(filename, ios::binary);
		if (!bmpIn.is_open())
		{
			return 1; //File open error code
		}

		BITMAPFILEHEADER bmfh;
		BITMAPINFOHEADER bmih;

		bmpIn.read((char*)&bmfh, sizeof(BITMAPFILEHEADER));  // int& x - ������;
		bmpIn.read((char*)&bmih, sizeof(BITMAPINFOHEADER));	 // int* x = &y - ������ ������

		int offset = 4 - (3 * bmih.biWidth) % 4;

		img.width = bmih.biWidth;
		img.height = bmih.biHeight;
		img.pixels = new RGB*[img.height];
		for (int i = 0; i < img.height; ++i)
		{
			img.pixels[i] = new RGB[img.width];
			for (int j = 0; j < img.width; ++j)
			{
				bmpIn.read((char*)&img.pixels[i][j], 3);
			}
			bmpIn.seekg(offset, ios::cur);
		}

		bmpIn.close();
		return 0;
	}
}

int WriteRgbImg(char filename[], const RgbImg& img)
{
	ofstream bmpOut;
	bmpOut.open(filename, ios::binary);
	if (!bmpOut.is_open())
	{
		return 1; //File open error code
	}

	int offset = 4 - (3 * img.width) % 4;

	BITMAPFILEHEADER bmfh;
	bmfh.bfReserved1 = 0;
	bmfh.bfReserved2 = 0;
	bmfh.bfType = (((WORD)'M') << 8) | 'B';
	bmfh.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);
	bmfh.bfSize = bmfh.bfOffBits + 3 * img.width*img.height + offset * img.height;

	BITMAPINFOHEADER bmih;
	bmih.biClrImportant = 0;
	bmih.biClrUsed = 0;
	bmih.biHeight = img.height;
	bmih.biWidth = img.width;
	bmih.biSize = sizeof(BITMAPINFOHEADER);
	bmih.biSizeImage = bmfh.bfSize - bmfh.bfOffBits;
	bmih.biCompression = 0;
	bmih.biPlanes = 1;
	bmih.biXPelsPerMeter = 1; //3779
	bmih.biYPelsPerMeter = 1; //3779
	bmih.biBitCount = 24;

	bmpOut.write((char*)&bmfh, sizeof(BITMAPFILEHEADER));
	bmpOut.write((char*)&bmih, sizeof(BITMAPINFOHEADER));

	BYTE* offsetArray = new BYTE[offset];
	for (int i = 0; i < offset; ++i)
		offsetArray[i] = 0;

	for (int i = 0; i < img.height; ++i)
	{
		for (int j = 0; j < img.width; ++j)
		{
			bmpOut.write((char*)&img.pixels[i][j], 3);
		}
		bmpOut.write((char*)offsetArray, offset);
	}

	delete[] offsetArray;
	bmpOut.close();
	return 0;
}

void ClearImgMemory(RgbImg & img)
{
	for (int row = 0; row < img.height; ++row)
		delete[] img.pixels[row];
	delete[] img.pixels;
}

void BlurImg(RgbImg& img, int kernel)
{
	int sum[3] = { 0 };
	int delimeter = 0;
	for (int row = 0; row < img.height; ++row)
		for (int col = 0; col < img.width; ++col)
		{
			for (int outerRow = row - kernel; outerRow <= row + kernel; ++outerRow)
				for (int outerCol = col - kernel; outerCol <= col + kernel; ++outerCol)
				{
					if ((outerRow < 0) || (outerRow > img.height - 1) || (outerCol < 0) || (outerCol > img.width - 1))
						continue;
					++delimeter;
					sum[0] += img.pixels[outerRow][outerCol].Red;
					sum[1] += img.pixels[outerRow][outerCol].Green;
					sum[2] += img.pixels[outerRow][outerCol].Blue;
				}
			img.pixels[row][col].Red = sum[0] / delimeter;
			img.pixels[row][col].Green = sum[1] / delimeter;
			img.pixels[row][col].Blue = sum[2] / delimeter;
			sum[0] = 0;
			sum[1] = 0;
			sum[2] = 0;
			delimeter = 0;
		}
}

#include <iostream>
void MedianFilterImg(RgbImg& img, int kernel)
{

	BYTE* sequence[3];
	int sequenceLength = 0;

	for (int i = 0; i < 3; ++i)
		sequence[i] = new BYTE[(2 * kernel + 1)*(2 * kernel + 1)];

	for (int row = 0; row < img.height; ++row)
		for (int col = 0; col < img.width; ++col)
		{
			for (int outerRow = row - kernel; outerRow <= row + kernel; ++outerRow)
				for (int outerCol = col - kernel; outerCol <= col + kernel; ++outerCol)
				{
					if ((outerRow < 0) || (outerRow > img.height - 1) || (outerCol < 0) || (outerCol > img.width - 1))
						continue;

					sequence[0][sequenceLength] = img.pixels[outerRow][outerCol].Red;
					sequence[1][sequenceLength] = img.pixels[outerRow][outerCol].Green;
					sequence[2][sequenceLength++] = img.pixels[outerRow][outerCol].Blue;
				}

			int middleIdx = sequenceLength / 2;

			for (int i = 0; i < 3; ++i)
				InsertionSort(sequence[i], sequenceLength);

			img.pixels[row][col].Red = sequence[0][middleIdx];
			img.pixels[row][col].Green = sequence[1][middleIdx];
			img.pixels[row][col].Blue = sequence[2][middleIdx];

			sequenceLength = 0;
		}

	for (int i = 0; i < 3; ++i)
		delete[] sequence[i];
}

void RgbToGray(RgbImg& img)
{
	int intensity;
	for (int row = 0; row < img.height; ++row)
		for (int col = 0; col < img.width; ++col)
		{	
			RGB& currPixel = img.pixels[row][col];
			intensity = (currPixel.Red
				+ currPixel.Green
				+ currPixel.Blue) / 3;
			currPixel.Red = intensity;
			currPixel.Green = intensity;
			currPixel.Blue = intensity;
		}
}

void InsertionSort(BYTE* array, int size)
{
	for (int i = size - 2; i >= 0; --i)
	{
		BYTE key = array[i];
		int j = i + 1;
		for (j; j < size; ++j)
		{
			if (key > array[j])
				array[j - 1] = array[j];
			else
				break;
		}
		array[j - 1] = key;
	}
}