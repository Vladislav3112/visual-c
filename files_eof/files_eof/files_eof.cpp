#include<iostream>
#include<fstream>
using namespace std;

void countSymbols(char filename[], int& ucCnt, int&lcCnt, int& sCnt)
{
	ucCnt = lcCnt = sCnt = 0;
	ifstream in;
	in.open(filename);
	if (!in.is_open())
	{
		throw runtime_error("failed to open file");
	}
	//another way
	//ignore EOF at the end 
	while (in.peek() != EOF)
		//while (!in.eof()) 
	{
		char curr = in.get();
		if (curr >= 65 && curr <= 90) ucCnt++;
		if (curr >= 97 && curr <= 122) lcCnt++;
		++sCnt;
		cout << (int)curr << " " << curr << endl;
	}
	in.close();
}
void countSymbolsString(char filename[], int& ucCnt, int&lcCnt, int& sCnt)
{
	char buffer[256];
	ucCnt = lcCnt = sCnt = 0;
	ifstream in;
	in.open(filename);
	if (!in.is_open())
	{
		throw runtime_error("failed to open file");
	}

	//another way
	//ignore EOF at the end 
	while (in.peek() != EOF)
		//while (!in.eof()) 
	{
		in.get(buffer,'\n');
		//in.getline(buffer, 256); //skips '\n' at the end
		int bufferSize = strlen(buffer);
		for (int i = 0; i < bufferSize; i++) {
			char curr = buffer[i];
			if (curr >= 65 && curr <= 90) ucCnt++;
			if (curr >= 97 && curr <= 122) lcCnt++;
			++sCnt;
			cout << (int)curr << " " << curr << endl;
		}
		in.get();
		++sCnt;
	}
	in.close();
}

int main(int args, char** argv)
{
	for (int i = 0; i < args; i++)
		cout << argv[i] << endl;
	int upperCaseCnt = 0;
	int lowerCaseCnt = 0;
	int symbolCount = 0;
	try {
		char filePath[] = "in.txt";
		countSymbolsString(filePath, upperCaseCnt, lowerCaseCnt, symbolCount);
	}
	catch (runtime_error error) 
	{
		cout << "ecxeption:" << error.what() << endl;
		system("pause");
		return -1;
	}

	cout << "upper case count" << upperCaseCnt << endl;
	cout << "lower case count" << lowerCaseCnt << endl;
	cout << "symbol count" << symbolCount << endl;
	
	system("pause");
	return 0;
}