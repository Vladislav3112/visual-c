#pragma once
#include "stroka.h"
static int numeral = 0;
struct Pair{
public:
	Pair(stroka eng="", stroka rus="") {
		word = eng;
		translate = rus;
		numeral++;
		code = numeral;	
	}
		
		stroka getWord() { return word; }
		stroka getTranslate() { return translate; }
		int getCode() { return code; }
		bool operator>(const Pair& pair)const {
			return(this->word > pair.word);
		};
		bool operator<(const Pair& pair)const {
			return(this->word < pair.word);
		};
		bool operator==(const Pair& pair)const {
			return(this->word == pair.word);
		};
		bool operator!=(const Pair& pair)const {
			return(this->word != pair.word);
		};
    private:
		
		int code;
		stroka word;
		stroka translate;
};