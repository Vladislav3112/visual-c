#include <iostream>
using namespace std;
int main() {
	int number_of_lines = 0;
	int count;
	int mod_2;
	cout << "write number of lines" << endl;
	cin >> number_of_lines;
	
	if (number_of_lines % 2 == 0) mod_2 = 1;
	else mod_2 = 0;

	int half = number_of_lines / 2;
	count = half;
	for (int i = 0; i < number_of_lines + mod_2; i++) {
	   
		if ((i == number_of_lines / 2) && (mod_2 == 1)) {
			half++;
			count--;
			continue;
		}
		for (int j = 0; j < count+1-mod_2 ; j++) {
			if ((j < half-mod_2)) cout << " ";
			else cout << "*";
		}
		if (i < number_of_lines / 2) {
			half--; 
			count++;
		}
		else { 
			half++;
			count--;
		}
		cout << "\n";	
	}
	system("pause");
	return 0;
}