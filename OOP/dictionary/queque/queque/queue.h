#pragma once
class queue {
	public:
		queue();
		queue(int n);
		queue(int a[], int n);
		queue(const queue& source);
		queue& push(int x);
		queue& pop();
		int& front();
		int getSize() const { return size; };

	private:
		int* data;
		int size;
		int capacity;

};