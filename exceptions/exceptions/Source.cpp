#include<iostream>
#include<exception>
using namespace std;

void printArray(const int* source, int size) {
	if (size < 0)
		throw range_error("negative size");
	for (int i = 0; i < size; i++)
		cout << source[i] << "\t";
	cout << endl;
}

//0 -- succes;
//-1 -- error
int printArrayWithErrorCode(const int* source, int size) {
	if (size < 0)
		return -1;
	for (int i = 0; i < size; i++)
		cout << source[i] << "\t";
	cout << endl;
	return 0;
}


int main() 
{
	int sourceArray[] = { 1,2,3 };
	try {
		printArray(sourceArray, -1);
	}
	catch (range_error e) {
		cout << "range error: " << e.what() << endl;
	}
	
	try 
	{
		int testVar = 1;
		if (testVar == 0)
			throw - 1;
		if (testVar == 1)
			throw "Error";
		cout << "end of try block" << endl;
	}
	catch (int error) {
		cout << error << endl;
	}
	catch (char* str) {
		cout <<"String err: "<< str << endl;
	}
	
	
	printArray(sourceArray, 3);
	system("pause");
	return 0;
}