template <typename Type>
void Swap(Type& a, Type& b)
{
	Type tmp = a;
	a = b;
	b = tmp;
}

template <typename Type>
bool greater1(Type a, Type b) 
{
	return a > b;
}

template <typename Type>
void bubbleSort(Type* array, int size, bool(*cmp)(Type, Type) = greater1<Type>)
{
	bool flag = false;
	for (int i = 0; i < size - 1; ++i)
	{
		flag = false;
		for (int j = 0; j < size - i - 1; ++j)
		{
			if (cmp(array[j],array[j + 1]))
			{
				flag = true;
				Swap(array[j], array[j + 1]);
			}
			
		}
		if (!flag) return;
	}

}

template <typename Type>
void selectionSort(Type* array, int size)
{
	for (int i = 0; i < size - 1; ++i)
	{
		int idx = 0;
		for (int j = 1; j < size - i; ++j)
		{
			if (array[j] > array[idx])
				idx = j;
		}
		Swap(array[idx], array[size - i - 1]);
	}
}

template <typename Type>
void insertionSort(Type* array, int size)
{
	for (int i = size - 2; i >= 0; --i)
	{
		Type key = array[i];
		int j = i + 1;
		for (j; j < size; ++j)
		{
			if (key > array[j])
				array[j - 1] = array[j];
			else
				break;
		}
		array[j - 1] = key;
	}
}

template<typename Type>
int binarySearch(Type* array, int size, Type value)
{
	int leftBound = 0;
	int rightBound = size - 1;

	while (leftBound < rightBound)
	{
		int currIdx = leftBound + (rightBound - leftBound) / 2;
		if (array[currIdx] > value)
			rightBound = currIdx - 1;
		else
			leftBound = currIdx + 1;
	}

	return leftBound;
}

template <typename Type>
void insertionSortBinary(Type* array, int size)
{
	for (int i = size - 2; i >= 0; --i)
	{
		Type key = array[i];
		int idx = binarySearch<Type>(array + i + 1, size - i - 1, key) + i + 1;
		int rightBound = key > array[idx] ? idx : idx - 1;

		for (int j = i + 1; j <= rightBound; ++j)
		{
			array[j - 1] = array[j];
		}
		array[rightBound] = key;
	}
}


template <typename Type>
void mergeSort(Type* array, int size,bool(*cmp)(Type,Type))
{
	if (size > 1)
	{
		int splitIdx = size / 2;
		mergeSort(array, splitIdx,cmp);
		mergeSort(array + splitIdx, size - splitIdx,cmp);
		merge(array, size, splitIdx);
	}
}

template <typename Type>
void merge(Type* array, int size, int splitIdx)
{
	int lPos = 0;
	int rPos = splitIdx;
	int pos = 0;

	Type* tmpSorted = new Type[size];
	while (lPos < splitIdx && rPos < size)
	{
		tmpSorted[pos++] = array[lPos] < array[rPos] ? array[lPos++] : array[rPos++];
	}

	while (lPos < splitIdx)
		tmpSorted[pos++] = array[lPos++];

	while (rPos < size)
		tmpSorted[pos++] = array[rPos++];

	for (int i = 0; i < size; ++i)
		array[i] = tmpSorted[i];

	delete[] tmpSorted;
}
//������� ���������� (Quick sort)
template <typename Type>
void qSort(Type* array, int size)
{
	if (size == 2)
	{
		if (array[0], array[1])
			swap(array[0], array[1]);
		return;
	}
	if (size <= 1)
		return;

	int keyIdx = size / 2;
	Type key = array[keyIdx];
	int lPos = 0;
	int rPos = size - 1;

	while (lPos < rPos)
	{
		while (key >= array[lPos] && lPos < keyIdx)
			++lPos;
		while (array[rPos] >= key && rPos > keyIdx)
			--rPos;
		if (lPos == rPos)
			break;

		swap(array[lPos], array[rPos]);

		if (lPos == keyIdx)
			keyIdx = rPos;

		else if (rPos == keyIdx)
			keyIdx = lPos;
	}
	qSort(array, keyIdx);
	qSort(array + keyIdx + 1, size - keyIdx - 1);
}
template <typename Type>
void heapSort(Type* array, int size, bool(*cmp)(Type, Type))
{
	//1. Make heap
	for (int i = size / 2 - 1; i >= 0; --i)
		heapElem(array, size, i, cmp);
	//2. Sort
	for (int i = 0; i < size; ++i)
	{
		swap(array[0], array[size - i - 1]);
		heapElem(array, size - i - 1, 0, cmp);
	}
}

template <typename Type>
void heapElem(Type* array, int size, int idx, bool(*cmp)(Type, Type))
{
	while (idx * 2 + 1 < size)
	{
		if (idx * 2 + 2 < size)
		{
			int maxIdx = cmp(array[idx * 2 + 1], array[idx * 2 + 2]) ? idx * 2 + 1 : idx * 2 + 2;
			if (cmp(array[maxIdx], array[idx]))
			{
				swap(array[maxIdx], array[idx]);
				idx = maxIdx;
			}
			else
				break;
		}
		else
		{
			if (cmp(array[2 * idx + 1], array[idx]))
			{
				swap(array[2 * idx + 1], array[idx]);
			}
			break;
		}
	}
}
