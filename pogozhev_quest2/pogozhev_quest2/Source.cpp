#include <iostream>
#include <fstream>
using namespace std;

char base64_chars[] =
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz"
"0123456789+/";
char code_name[] = "Emily";
int a[5];
int main()
{
	setlocale(LC_ALL, "Ru");
	ifstream in;
	in.open("rot3.txt");
	if (!in.is_open())
	{
		cout << "Input file error!" << endl;
		system("pause");
		return 0;
	}
	//name to indices
	for (int j = 0; j < 5; j++)
		for (int i = 0; i < 64; i++) {
			if (base64_chars[i] == code_name[j])
				a[j] = i;
	
	}

	ofstream out;
	out.open("out.txt");
	if (!out.is_open())
	{
		cout << "Output file error!" << endl;
		system("pause");
		return 0;
	}
	int idx = -1;
	//rot3
	while (in.peek()!=EOF) {
		char c = in.get();
		idx = -1;
		for (int i = 0; i < 64; i++) {
			if (c == base64_chars[i])
			{
				idx = i;
			}
		}
		if (idx == -1) {
			out << c;
			continue; 
		}
		idx -= 3;
		if (idx < 0) idx += 64;
		out << base64_chars[idx];
	}

	//vineger
	/*int rot_idx = -1;
	int rot_cnt = 0;
	while (in.peek() != EOF) {
		
		rot_idx = a[rot_cnt];
		char c = in.get();
		idx = -1;
		for (int i = 0; i < 64; i++) {
			if (c == base64_chars[i])
			{
				idx = i;
			}
		}
		if (idx == -1) {
			out << c;
			continue;
		}
		idx -= rot_idx;
		if (idx < 0) idx += 64;
		out << base64_chars[idx];
		rot_cnt++;
		if (rot_cnt == 5) rot_cnt = 0;
	}*/

	out.close();
	in.close();


	system("pause");
	return 0;
}