#include <iostream>
using namespace std;

int sum(int a, int  b)
{
	return a + b;
}

void swap_ptr(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
	//return; - ���������� ������� void
}
void swap_link(int& a, int& b ) 
{
	int tmp = a;
	a = b;
	b = tmp;
	//return; - ���������� ������� void
}

int main() {

	int megatron9000 = 9000;
	int*ptr = &megatron9000;
	int&link = megatron9000; //2 names one var: link � megatron9000 
	link = 9001;
	cout << megatron9000 << endl;
    int arr[5]={1,1,2,3,5};
	int& middle = arr[2];
	middle = 3;
	cout << arr[2] << endl;

	int x = 2;
	int y = 3; 
	swap_link(x,y);
	cout << x <<" "<< y << endl;
	swap_ptr(&x,&y);
	cout << x << " " << y << endl;
	cout << sum(3, 5) << endl;
	system("pause");
	return 0;
}