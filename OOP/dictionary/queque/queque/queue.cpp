#include "queue.h"

queue::queue()
{
	data = new int[1];
	capacity = 1;
	size = 0;
}

queue::queue(int n)
{
	if (n < 0)
		throw "size is less than 0";
	capacity = n;
	size = 0;
	data = new int[capacity];
}

queue::queue(int  source[], int n)
{
	if (n < 0)
		throw "size is less than 0";
	capacity = n;
	size = capacity;
	data = new int[n];
	for (int i = 0; i < n; i++)
		data[i] = source[i];
}

queue::queue(const queue & source) : capacity(source.capacity),size(source.capacity)
{
	data = new int[capacity];
	for (int i = 0; i < size; i++)
		data[i] = source.data[i];
}

queue & queue::push(int x)
{
	if (size < capacity)
	{
		data[size++] = x;
		return *this;
	}
	capacity *= 2;
	int* tmp = new int[capacity];
	for (int i = 0; i < size; ++i)
		tmp[i] = data[i];
	tmp[size++] = x;
	delete[] data;
	data = tmp;
	return *this;
}

queue & queue::pop()
{
	if (size <= 0)throw "size is  0";
	for (int i = 0; i < size - 1; i++) {
		data[i] = data[i + 1];
	}
	data[size - 1] = 0;
	size--;
	return *this;
}

int & queue::front()
{
	return data[0];
}


