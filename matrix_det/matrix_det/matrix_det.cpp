#include<iostream>
using namespace std;

template <typename Type>
void randomizeArray(Type* sourceAray[], int rows) {
	for (int row = 0; row < rows; ++row)
		for (int col = 0; col < rows; ++col)
			sourceAray[row][col] = rand()%20 - 10;
}
template <typename Type>
int Matrix_det_by_rows(Type* sourceAray[], int rows) {
	int det = 0;
	int m = 1;
	if (rows == 1) det *= sourceAray[0][0];
	else 
		for (int row = 0; row < rows; ++row)
		{
			for (int col = 0; col < rows; ++col)
			{
			det += (int)pow(-1, 2 + col)*sourceAray[0][row] *
				Matrix_det_by_rows(sourceAray,rows-1);
			}
		}
	return det;
}
template <typename Type>
int Matrix_det_Gauss(Type* sourceArray[], int rows) {
	double det = 1;
	int m = 0;
	double str_num = 0;
	double diag_elem = 0;
	for (int row = 0; row < rows-1; ++row)
	{
		
		det *= sourceArray[row][row];
		diag_elem = (double)sourceArray[row][row];
		for (int col = 0; col < rows; ++col) {

			sourceArray[row][col] /= (double)diag_elem; //����� ������ ������� row �� row �������(!=0)

			str_num = sourceArray[row + 1][col];
			

				for (int i = row + 1; i < rows; ++i)
			{
				for (int j = col; j < rows; ++j)                   //����� �� ������ ���������������[row][j]
					sourceArray[i][j] -= str_num * sourceArray[row][j]; // �������� row �����(�row) �� �����������  
			}
		
		}
	}
	det *= sourceArray[rows-1][rows-1];
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < rows; j++) {
		
			cout << sourceArray[i][j]<<" ";
		}
		cout << "\n";
	}
		
	return det;
	
}
template <typename Type>
void printArray(Type* sourceArray[],int rows) {
	for (int row = 0; row < rows; ++row)
	{
		for (int col = 0; col < rows; ++col)
			cout << sourceArray[row][col] << '\t';
		cout << endl;
	}
}

int main() 
{
	int det;
	int rows;
	cout << "enter number of rows" << endl;
	cin >> rows;
	double** matrix = new double*[rows];
	for (int i = 0; i < rows; i++)
		matrix[i] = new double[rows];
	
	randomizeArray(matrix, rows);
	printArray(matrix, rows);
	cout << Matrix_det_Gauss(matrix, rows);

	for (int i = 0; i < rows; ++i)
		delete[] matrix[i];
	delete[] matrix;
	system("pause");
	return 0;
}