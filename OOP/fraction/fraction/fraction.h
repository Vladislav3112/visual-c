#pragma once
class fraction {
public :
	int getNumerator()const { return numerator; }
	int getDeominator()const { return denominator; }
	fraction();
	fraction(int a, int b);
	fraction& reduce();
	int nod(int n, int m);
	fraction sum(fraction f);
	fraction sub(fraction f);
	fraction mult(fraction f);
	fraction div(fraction f);
	fraction& check_sign();

private:
	int numerator;
	int denominator;
};