#include<iostream>
using namespace std;

template<typename Type>
class BinaryTree {
	private:
		struct Node {//parent �� �������� -??
			Node* parent;
			Node* left;
			Node* right;
			Type data;

			Node(Type _data) : left(nullptr),
							   right(nullptr),
							   parent(nullptr),
							data(_data) {}
		};
		Node* root;
		int size;

		void addItemSubTree(const Type& item, Node* subroot);
		void printSubTree(Node* subroot) const;
		void deleteSubTree(Node* subroot);

public:
	BinaryTree() :root(nullptr), size(0) {}
	~BinaryTree() {
		if (!root)return;
		deleteSubTree(root);
	}
	int getSize() const{ return size; }
	void insert(const Type& data);
	void print()const {
		if (!root)return;
		printSubTree(root);
	};


	class Iterator {
	private:
		Node* current;
		Node* left;
		Node* right;
		Node* parent;
	public:
		Iterator(Node* curr = nullptr, Node* r = nullptr, Node* l = nullptr, Node* p = nullptr) :current(curr),
																			  left(l),
																			  right(r),
			parent(p) {}

		Type& operator*() const { return current->data; }
		bool operator==(const Iterator& right) { return current == right.current; }
		bool operator!=(const Iterator& right) { return current != right.current; }

		Iterator& operator++() {
			if (!current)throw "Iterator out of bounds";
			if (left != nullptr)current = current->left;
				else if (right != nullptr)current = current->right;
					else if (current->parent->data >= current->data) {//�������� �� ������/����� ����������
				while (current->parent->right == nullptr)current = current->parent;//����� ���������
				current = current->right->right;
			}
					else if (current->parent->right != nullptr)current = current->parent->right;//������ ���������
					else current = current->right->right;//nullptr
			return *this;
		}

		Iterator operator++(int n) {
			if (!current)throw "Iterator out of bounds";
			Node* tmp = current;
			if (left != nullptr)current = current->left;
				else if (right != nullptr)current = current->right;
					else if (current->parent->data >= current->data) {//�������� �� ������/����� ����������
					while (current->parent->right == nullptr)current = current->parent;//����� ���������
					current = current->right;
			}
				else if (current->parent->right != nullptr)current = current->patent->right;
				else current = current->right;//nullptr
			return Iterator(tmp);
		}
	};
	Iterator begin() const { return Iterator(root); }
	Iterator end() const { return Iterator(); }
	Iterator find(const Type& data)const;
};

template<typename Type>
void BinaryTree<Type>::printSubTree(Node* subroot) const {
	if (subroot->left) printSubTree(subroot->left);
	cout << subroot->data << endl;
	if (subroot->right) printSubTree(subroot->right);
}

template<typename Type>
void BinaryTree<Type>::deleteSubTree(Node* subroot){
	if (subroot->left) deleteSubTree(subroot->left);
	if (subroot->right) deleteSubTree(subroot->right);
	delete subroot;
}

template<typename Type>
void BinaryTree<Type>::insert(const Type& data) {
	if (!root){
		root = new Node(data);
		++size;
		return;
	}
	addItemSubTree(data, root);
}


template<typename Type>
void BinaryTree<Type>::addItemSubTree(const Type& item, Node* subtree)
{
	if (item > subtree->data) {
		if (subtree->right == nullptr) {
			subtree->right = new Node(item);
			++size;
			return;
		}
		addItemSubTree(item, subtree->right);
	}
	else
	{
		if (subtree->left == nullptr) {
			subtree->left = new Node(item);
			++size;
			return;	
		}
		addItemSubTree(item, subtree->left);
	}
}

int main() {
	BinaryTree<int> tree;
	tree.insert(5);
	tree.insert(3);
	tree.insert(1);
	tree.insert(2);
	tree.insert(6);
	tree.insert(7);
	tree.insert(8);
	tree.print();
	for (auto current = tree.begin(); current != tree.end(); ++current)
		cout << *current << endl;

	system("pause");
	return 0;
}