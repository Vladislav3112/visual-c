﻿#include <iostream>
#include "avl_tree.h"
using namespace std;


int main()
{
	AvlTree<int> tree;
	for (int i = 0; i < 10; ++i)
		tree.insert(i);
	tree.print();
	cout <<"\n";
	for (auto current = tree.begin(); current != tree.end(); ++current)
		cout << *current << endl;
	auto foundElem = tree.find(5);
	if (foundElem != tree.end())
		cout <<"found elem: " << *foundElem<< endl;

	system("pause");
	return 0;
}
