#pragma once
#include "math.h"
class complex {
public:
	    complex();
		complex(double a, double b);
		complex(const complex& source);
		double getReal()const;
		double getImagine()const;
		complex sum(complex c1);
		complex sub(complex c1);
		complex conj();
		complex mult(complex c1);
		double abs1();
		double arg(); 
		void print()const;
		complex& operator=(const complex& c);
		complex& operator++();//prefix
		complex operator++(int);//postfix ,int ����� ��� �������, �� ������������
		static complex testMethod();
		static complex& testRefMethod();

private:
	double imagine;
	double real;
};