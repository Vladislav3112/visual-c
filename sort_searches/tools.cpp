#include "tools.h"
#include <iostream>
#include<cstring>
#include<functional>
using namespace std;

void printArray(const int* source, int size) {
	for (int i = 0;i < size;i++)
		cout << source[i] << '\t';
	cout << endl;
}
void randArray(int* source, int size, int maxVal, int minVal) {

	for (int i = 0; i < size; i++)
		source[i] = minVal + rand() % (maxVal - minVal + 1);
}
void copyArray(const int* source, int* dest, int size) {
	for (int i = 0; i < size; i++)
		dest[i] = source[i];
}
bool strGreater(char strLeft[], char strRight[]) 
{
	return strcmp(strLeft, strRight) == 1;
}
double f1(double x) {
	return x * x;
}
double f2(double x) {
	return 2 * x;
}
double fSum(double x) {
	return f1(x) + f2(x);
}
function<double(double)> genFunc(function<double(double)> f1, function<double(double)> f2)
{
	return [=](double x) { return f1(x) + f2(x); }; //[=] - ��� ��������� (�� ����.)
}
void testFunc() {
	function<double(double)> sumFunc = genFunc(f1,f2);
	double x = 2;
	double y = sumFunc(x);
}

function<void()> closureFail()
{
	int y = 25;
	return [&y]() {cout << y << endl; };//�� ������
}
void testClosureFail()
{
	auto failFunc = closureFail();//����� ������� � � �������
	failFunc();//y ��� �� ����������
}
