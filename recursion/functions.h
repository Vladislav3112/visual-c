#ifndef FUNCTIONS_H
#define FUNCTIONS_H
int powerIter(int val, int deg);
double powerIter(double val, double deg);
int powerRecursive(int val, int deg);
void hanoy(int size, int from, int to);
int counter();
#endif