#include<iostream>
#include<time.h>
using namespace std;
template <typename Type>

void randomizeArray(Type* sourceArray, int size)
{
	srand(time(NULL));
	for (int i = 0; i < size; i++) 
		sourceArray[i] = (Type)(10 * ((double)rand() / RAND_MAX));
}

template <typename Type>
void reverseArray(Type* sourceArray, int size)
{
	int tmp = 0;
	for (int i = 0; i < size / 2; i++) {
		tmp = sourceArray[i];
		sourceArray[i] = sourceArray[size - i - 1];
		sourceArray[size - i - 1] = tmp;
	}
}
template <typename Type>
void change_0_in_Array( Type* sourceArray, int size)
{
	//TODO this
	int count = 0;
	int k = 0;
	int ind_0 = -1;
	int tmp = -1;
	for (int i = 0; i < size; i++)
		if (sourceArray[i] == 0)count++;
	k = count;
	for (int i = 0; i < size-1; i++) {
		if (i == ind_0) {
			k--;
			continue;
		}
		if ((i == size - count) && (k == 1)) {
			sourceArray[size-1] = tmp;
			break;
		}
			tmp = sourceArray[i + k];
			sourceArray[i + k] = sourceArray[i];
			if (tmp == 0) {
				ind_0 = i+k;
				tmp = -1;
			}
		
		}
	for (int i = 0; i < count; i++)
		sourceArray[i] = 0;

}

template <typename Type>
void printArray(const Type* sourceArray, int size) 
{
	for (int i = 0; i < size; i++) 
		cout << sourceArray[i] << "\t";
}

int main() {
	int n;

	cout << "enter number of elements";
	cin >> n;
	int *a = new int[n];
	for (int i = 0; i < n; i++) 
	{
		cout << "enter " << i << " element ";
		cin >> a[i];
	}

	//randomizeArray(a, n);
	change_0_in_Array(a, n);
	cout << "result array:";
	printArray(a, n);


	//cout << "reversed array:";
	//printArray(a, n);

	delete[]a;
	system("pause");
	return 0;
	
}