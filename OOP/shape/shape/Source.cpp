#include <iostream>
using namespace std;

class Shape {
public:
	Shape() { cout << "Shape was created" << endl; }
	virtual ~Shape() { cout << "Shape was destroyed" << endl; }
	virtual void show() = 0;//��� ����������� ������ �������� ���� ���������
	virtual void hide() = 0;//����� ����. ����� -> ����������� �����;
							//o�������� �������������� ����� �� ���� �����������
};
class Point : public Shape {//��� �� ����������
protected:
	int x;
	int y;
public:
	Point(int x1 = 0, int y1 = 0) : x(x1), y(y1) {
		cout << "Point was created" << endl;
	}
	~Point() { cout << "Point was destroyed" << endl; }
	void show() {
		cout << "Point is shown at (";
		cout << x << "," << y << ")" << endl;
	}
	void hide() {
		cout << "Point is hidden at (";
		cout << x << "," << y << ")" << endl;
	}
	void move(int dx = 0, int dy = 0) {
		x += dx;
		y += dy;
	}
};
class Circle : virtual public Point {
protected:
	int radius;
public:
	Circle(int x1 = 0, int y1 = 0, int r = 1) :Point(x1, y1), radius(r) {//����� ����� ���������������� const
		//x = x1;
		cout << "Circle was created" << endl;
	}
	void show() {
		cout << "Circle is shown at (";
		cout << x << "," << y << "," << radius << ")" << endl;
	}
	~Circle() { cout << "Circle was destroyed" << endl; }
	//void move(int dx, int dy) override{
	//- �������� �� ���������������, ������ ��� virtual
   // }
};
class Rectangle : virtual public Point {
protected:
	int width;
	int height;
public:
	Rectangle(int x1 = 0, int y1 = 0, int w = 1, int h = 1) :Point(x1, y1), width(w), height(h) {//����� ����� ���������������� const
		//x = x1;
		cout << "Rectangle was created" << endl;
	}
	void show() {
		cout << "Rectangle is shown at (";
		cout << x << "," << y << "," << width << "," << height << ")" << endl;
	}
	~Rectangle() { cout << "Rectangle was created" << endl; }
};

class CircleInRectangle :public Circle, public Rectangle 
{
public:
	CircleInRectangle(int x = 0, int y = 0, int r = 0):
													//Point(x,y),
													Circle(x,y,r),
													Rectangle(x,y,r,r)
	{
		//
	}
	void show() { 
		Circle::show();
		Rectangle::show();
		move(); // ��� �� �������� ����������,��� �� ����������� ������������
	}
	void hide() {
		Circle::hide();
		Rectangle::hide();
	}
};


int main() {
	//Shape* shape = new Shape(); error, �.�. ����� ����. ����� 
	Point* point = new Point();
	Circle* circle = new Circle(2, 3, 5);
	Rectangle* rect = new Rectangle();

	const int shapesLength = 3;
	Shape* shapes[shapesLength];
	shapes[0] = new Circle();
	shapes[1] = new Point();
	shapes[2] = new Rectangle();
	for (int i = 0; i < shapesLength; i++) {
		shapes[i]->show(); //shape is shown, ����� ���������� virtual - �����������
		Point* objPtr = dynamic_cast<Point*>(shapes[i]);
		if (objPtr != nullptr)objPtr->move(10, 10);//����� � point ��� move, nullptr ��� ���� �������������� �� �� point
	}
	delete point;
	delete circle;
	delete rect;
	for (int i = 0; i < shapesLength; i++)delete shapes[i];
	system("pause");
}
//����������� ������������ -
//				 Shape
//				   |
//				 Point
//			 /		      \
//       Circle 	    Rectangle
//			 \			  /
//		   CircleInRectangle