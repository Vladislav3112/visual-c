#include <iostream>
#include "sorts.h"
#include"time.h"
#include "tools.h"
#include <chrono>

using namespace std;
using namespace std::chrono;

int globalX = 5;
static high_resolution_clock::time_point sstart;//static ���������� ������ � ���� �����
void tic() {
	sstart = high_resolution_clock::now();
}
long toc() {
	return duration_cast<milliseconds>(high_resolution_clock::now() - sstart).count();
}

template <typename Type>
void testSort(char sortName[],
	Type* source,
	Type size,
	void(*sortFunc)(Type*, int, bool(*)(Type, Type)),
	bool(*cmpFunc)(Type, Type) = greater1)
{
	int* tmpArr = new int[size];
	long finishToc;
	cout << sortName << endl;
	copyArray(source, tmpArr, size);
	tic();
	sortFunc(tmpArr, size, cmpFunc);
	finishToc = toc();
	cout << "time passed: " << finishToc << endl;
}


int main()
{
	srand(time(NULL));
	const int SIZE = 10000;
	int* arr = new int[SIZE];
	int* SourceArr = new int[SIZE];
	long finishToc;
	clock_t start;
	clock_t finish;
	randArray(arr, SIZE, SIZE);

	testSort("Bubble sort", arr, SIZE, bubbleSort<int>);
	testSort("Merge sort", arr, SIZE, mergeSort<int>);
	testSort("heap sort", arr, SIZE, heapSort<int>);

	//bubbleSort(arr, SIZE, greater);
	bubbleSort<int>(arr, SIZE, [](int a, int b) {return a > b; }); // ��� ���������(���������)

	

	//string sort
	/*char strArray[4][5] = { "str3", "str1","str2","abc" };
	char** strDynamic = new char* [4];
	for (int i = 0; i < 4; i++)
		strDynamic[i] = strArray[i];

	cout << "C-string array:" << endl;
	for (int i = 0; i < 4; i++) 
		cout << strDynamic[i] << endl;

	bubbleSort(strDynamic, 4, strGreater);
	
	cout << "C-string sorted array:" << endl;
	for (int i = 0; i < 4; i++)
		cout << strDynamic[i] << endl;*/

	delete[] SourceArr;
	delete[] arr;
	system("pause");
	return 0;
}