#include <iostream>
using namespace std;

int main() 
{
	int rows;
	int cols;
	cout << "enter rows and cols: ";
	cin >> rows >> cols;
	int** matrix = new int*[rows]; 
	for (int i = 0; i < rows; i++)
		matrix[i] = new int[cols];
	//������ � cols ��������(������ 2d) --> rows ������� �� ������ --> matrix 
	//input
	for (int row = 0; row < rows; row++)
		for (int col = 0; col < cols; col++)
			matrix[row][col] = row*cols + col + 1;
	
	//output
	for (int row = 0; row < rows; row++)
	{
		for (int col = 0; col < cols; col++)
			cout << matrix[row][col] << '\t';
		cout << endl;
	}

	//Cleaning the memory
	for (int i = 0; i < rows; i++)
		delete[] matrix[i];
	delete[] matrix;
	
	system("pause");
	return 0;
}