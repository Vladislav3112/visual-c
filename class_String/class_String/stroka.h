#pragma once
#include <iostream>
#include <string.h>

class stroka {
	int _size;
	char *data;
public:
	stroka(const char *str);
	stroka(int N);
	stroka(const stroka& str);
	~stroka();
	int size()const { return _size; }
	int length()const { return strlen(data); }
	const char* str() const { return data; }
	char & operator[](int i)const { return data[i]; }
	stroka& operator=(const stroka&);
	stroka operator+(const stroka& str)const;
	bool operator==(const stroka& X)const;
	bool operator!=(const stroka& X)const;
	bool operator<(const stroka& X)const;

};
stroka::stroka(const char *str = "") {
	_size = strlen(str) + 1;
	data = new char[_size];
	for (int i = 0; i < _size - 1; i++)
		data[i] = str[i];
	data[_size - 1] = '\0';
}

stroka::stroka(int N)
{
	data = new char(N);
	_size = N;
}

stroka::stroka(const stroka& str) :_size(str._size)
{
	data = new char[str.length() + 1];
	for (int i = 0; i <= str.length(); i++)
		data[i] = str.data[i];

}

stroka::~stroka()
{
	delete[] data;
}

stroka & stroka::operator=(const stroka &str)
{
	data = new char[str.length() + 1];
	for (int i = 0; i <= str.length(); i++)
		data[i] = str.data[i];
	return *this;
}

stroka stroka::operator+(const stroka & str) const
{
	int size_sum = this->length() + str.length() + 1;
	char* data_sum = new char[size_sum];
	strcpy_s(data_sum, size_sum, this->data);
	strcat_s(data_sum, size_sum, str.data);
	return stroka(data_sum);
}

bool stroka::operator==(const stroka& X)const { ///TODO
	int i = strcmp(this->data, X.data);
	if (i != 0)return false;
	else return true;
}
bool stroka::operator!=(const stroka& X)const {///TODO
	int i = strcmp(this->data, X.data);
	if (i != 0)return true;
	else return false;
}
bool stroka::operator<(const stroka& X)const {
	int i = strcmp(this->data, X.data);
	if (i < 0)return true;
	else return false;
}
