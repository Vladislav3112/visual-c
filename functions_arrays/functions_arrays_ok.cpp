#include<iostream>
#include<time.h>
using namespace std;

double* createArray(int size)
{
	return new double[size];
}
//function assigns random values
void randomArray(double sourceArray[], int size) 
{
	for (int i = 0; i < size; i++)
		sourceArray[i] = 10 * ((double)rand() / RAND_MAX);
}
double findMaxElem(const double sourceArray[], int size) {
	double max_elem = sourceArray[0];
	for (int i = 1; i < size; i++)
		if (sourceArray[i] > max_elem)
			max_elem = sourceArray[i];
	return max_elem;
}
void printArray(const double sourceArray[], int size) {
	for (int i = 0; i < size; i++)
		cout << sourceArray[i] << '\t';
	cout << endl;


} 
// const �� ��������� �������� ������
void cleanArray(const double sourceArray[])
{
	delete[] sourceArray;
}
void initData(double* &sourceArray, int &size) {//cc���� ��������� �������� �� ��������
	srand(time(NULL));
	cout << "Enter array size: " << endl;
	cin >> size;
	sourceArray = createArray(size);
	randomArray(sourceArray, size);
}

int main_ok()
{
	//variables
	int size;
	double max_elem;
	double *randArray;

	//initialization
	initData(randArray, size);

	//logic
	max_elem = findMaxElem(randArray, size);

	//output
	printArray(randArray, size);
	cout << "Max element = " << max_elem << endl;

	//cleaning
	cleanArray(randArray);

	system("pause");
	return 0;
}