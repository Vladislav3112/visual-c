#include<iostream>
#include<time.h>
using namespace std;
template <typename Type1>

void shift_massive(Type1 * sourceArray, int size, int shift)
{
	shift = shift % size;

	for (int i = size - 1; i >= 0; i--)
		sourceArray[i + shift] = sourceArray[i];
	for (int i =0; i < shift; i++)
		sourceArray[i] = sourceArray[i + size];

}
template <typename Type1>
void randomArray(Type1 sourceArray[], int size)
{
	for (int i = 0; i < size; i++)
		sourceArray[i] = (Type1)(10 * ((double)rand() / RAND_MAX));
}
template <typename Type1>
void printArray(Type1 * sourceArray, int size)
{
	for (int i = 0; i < size; i++)
		cout << sourceArray[i] << "\t";
	cout << endl;
}


int main() 
{
	int size;
	int step;
	
	srand(time(NULL));
	cout << "enter number of elements and step";
	cin >> size >> step;
	
	double *a = new double[size*2];
	
	cout << "init massive:" << endl;
	randomArray(a, size);
	printArray(a, size);
	shift_massive(a, size, step);

	cout << "shifted massive:" << endl;
	printArray(a, size);

	delete[] a;
	system("pause");
}