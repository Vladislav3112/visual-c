#include<iostream>
#include<fstream>
using namespace std;


int main()
{
	ifstream in;
	in.open("in.txt");
	if (!in.is_open())
	{
		cout << "failed to open file";
		system("pause");
		return -1;
	}	

	int rows;
	int cols;
	in >> rows >> cols;
	int** matrix = new int*[rows];
	for (int i = 0; i < rows; i++)
		matrix[i] = new int[cols];


	ofstream out;
	out.open("out.txt");
	if (!out.is_open())
	{
		cout << "failed to open out file";
		system("pause");
		return -1;
	}

	for (int i = 0; i < rows; i++)
		for (int j = 0; j < cols; j++)
			in >> matrix[i][j];

	out << rows << " " << cols << endl;
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++)
			out << matrix[i][j] << " ";
		out << endl;
	}

	for (int i = 0; i < rows; i++)
		delete matrix[i];
	delete[] matrix;

	out.close();
	in.close();
	system("pause");
	return 0;
}