#include <iostream>
#include <fstream>
#include "Base64.h"
using namespace std;

int main()
{
	int equal_count = 0;
	setlocale(LC_ALL, "Ru");
	//������� ����
	ifstream in;
	in.open("base64.txt");
	if (!in.is_open())
	{
		cout << "Input file error!" << endl;
		system("pause");
		return 0;
	}
	//������ ������ �����
	in.seekg(0, ios::end);
	int fSize = in.tellg();
	
	cout << "������ ��������� ���������: ";
	cout << fSize << endl << endl;

	//������������ � ������ �����
	in.seekg(0, ios::beg);

	//��������� ��� � ������
	char* fileContent = new char[fSize];
	in.read(fileContent, fSize);

	//���������� ������ �� BASE64
	int size;
	char* decoded = base64_decode(fileContent, fSize, size);
	

	cout << "������ ���������������� ���������: ";
	cout << size-1 << endl << endl;

	//�������� ����
	ofstream out;
	out.open("out.eml",ios::binary);
	if (!out.is_open())
	{
		cout << "Output file error!" << endl;
		system("pause");
		return 0;
	}
	out.write(decoded, size-1);

	//��������� �����
	out.close();
	in.close();

	//������� ������
	delete[] fileContent;
	delete[] decoded;

	system("pause");
	return 0;
}