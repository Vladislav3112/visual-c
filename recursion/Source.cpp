#include<iostream>
#include "functions.h"
using namespace std;
int powerIter(int val, int deg)
{
	cout << "power integer" << endl;
	int result = 1;
	for (int i = 0; i < abs(deg); i++)
		(deg >= 0) ? result *= val : result /= val;
	return result;
}

double powerIter(double val, double deg) {
	cout << "power double" << endl;
	return exp(deg*log(abs(val)));
}

int powerRecursive(int val, int deg)
{
	if (deg == 0)
		return 1;
	return val*powerRecursive(val, deg - 1);
}

void hanoy(int size, int from, int to)
{
	if (size > 1)
	{
		int tmp = 6 - from - to;
		hanoy(size - 1, from, tmp);
		hanoy(1, from, to);
		hanoy(size - 1, tmp, to);
	}
	else
		cout << from << "->" << to << endl;
}

int counter() 
{
	static int x = 0; //������������� ������ 1 ���
	cout << ++x << endl;
	return x;
}