#include<iostream>
using namespace std;

double f1(int* array,int count){
	int sum = 0;
	for (int i = 0; i < count; i++) {
		sum += array[i];
	}
	return sum;
}

double f2(int* array,int count) {
	int max = array[0];
	int min = max;
	for (int i = 1; i < count; i++) {
		if (array[i] > max)
			max = array[i];
		if (array[i] < min)min = array[i];
	}
	return(min);
}

double f3(int* array,int count) {
	double sum = 0.0;
	for (int i = 1; i < count; i++) {
		sum += array[i];
	}
	sum /=count;
	return(sum);
}
void print_changed_array(double(*func)(int*,int),int* array,int count) {
	
	cout << func(array, count)<<" ";
	for (int i = 1; i < count; i++) {
		cout << array[i] << " ";
	}

}
int main() {
	int a[8] = {1,4,7,2342,-224,42,676,9};
	print_changed_array(f2, a, 8);

	system("pause");
	return 0;
}