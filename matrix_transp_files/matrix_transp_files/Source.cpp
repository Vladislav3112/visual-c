#include <iostream>
#include <fstream>
#include<exception>
using namespace std;
int main(int argc, char** argv)
{
	try {
		if (argc == 3) {
			for (int i = 0; i < argc; ++i)
				cout << argv[i] << endl;
		}else throw exception("incorrect arguments");
	}
	catch (exception e) {
		cout <<e.what() << endl;
	}
	

	int rows, cols;
	ofstream out;
	ifstream in;
	in.open(argv[1]);
	out.open(argv[2]);
	in >> cols >> rows;
	

	int**matrix = new int*[rows];
	for (int i = 0; i <rows;i++)
		matrix[i] = new int[cols];

	for (int i = 0; i < rows; i++) 
		for (int j = 0; j < cols; j++) {
			in >> matrix[rows - i - 1][cols - j - 1];
		}
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			out << matrix[i][j]<<" ";
		}
		out << "\n";
	}


	in.close();
	out.close();
	for (int i = 0; i < rows; ++i)
		delete[] matrix[i];
	delete[] matrix;
	system("pause");
	return 0;
}