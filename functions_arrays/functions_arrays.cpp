#include<iostream>
#include<time.h>
using namespace std;

int mainOlder()
{
	
	int size;
	double max_elem;
	double *randArray;

	srand(time(NULL));
	cout << "Enter array size: "<< endl;
	cin >> size;
	randArray = new double[size];
	for (int i = 0; i < size; i++)
		randArray[i] = 10 * ((double)rand()/RAND_MAX);

	max_elem = randArray[0];
	for (int i = 1; i < size; i++)
		if (randArray[i] > max_elem)
			max_elem = randArray[i];


	for (int i = 0; i < size; i++)
		cout << randArray[i] << '\t';
	cout << endl;

	cout << "Max element = " << max_elem << endl;
	
	delete[] randArray;

	system("pause");
	return 0;
}