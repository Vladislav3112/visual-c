#pragma once
class vector;//���������� ���������� ��� �����
class TestFriend {
public:
	void testMethod(vector& vec) {
		//vec.size=10;
	}
};

class vector {	//struct
public:
	//����������� �� ���������
	vector();
	vector(int Size);
	vector(int source[], int Size);
	vector(const vector& source);

	vector& push_back(int val);
	int pop_back();
	int& getElemAt(int index) const;
	int getSize() const;
	~vector();
	void print() const;
	static int getObjCount();
	//friend std::ostream& operator<<(ostream& out, const vector& vec);//��� ������ � ��������� �����
	friend TestFriend;//������������� �����
private:
	int* data;
	int size;
	int capacity;
	static int objCount;//�� ����������� ������
};

//void printArray(vector& vec) {
//	for (int i = 0; i < vec.size; i++)
//		cout << vec.data[i] << " ";
//}

