#include "Base64.h"
#include <iostream>
#include <string>

char base64_chars[] =
"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
"abcdefghijklmnopqrstuvwxyz"
"0123456789+/";

char* base64_decode(char* message, int messageSize, int& decodedSize)
{

	//c����� base 64 
	char decoded_char;
	//������� ������ �������������� ������
	decodedSize = 3 * (messageSize / 4) + 1;
	
	//�������� ������
	char* decodedStr = new char[decodedSize];

	//������ ������ � C-�����
	decodedStr[decodedSize - 1] = '\0';

	//����� ��� ������� ��������
	char source[4];

	//������� �������� �������� ������������������
	char indices[3];

	//������� �������� ��������
	int counter = messageSize;

	//������ ��� �������� ������
	int idx = 0;

	//������ ��� ��������� ���������
	int encodedStrIdx = 0;
	
	//��������� �������� �����
	while (counter--)
	{
		decoded_char = *(message)++;
		if (decoded_char == '=') {
			decoded_char = 0;
			decodedSize--;
		}
		else
			for (int i = 0; i < 64; i++) {
				if (decoded_char == base64_chars[i])
				{
					source[idx] = i;//������ �� �������� - A->1
					idx++;
				}
			}
		//������������ ������ ������ ������ ������ �������
		if (idx == 4)
		{
			idx = 0;
			
			//��������� �� 3 ������������� �������
			indices[0] = source[0] * 4 + (source[1] >> 4);//�����
			indices[1] = (source[1] << 4) + (source[2] >> 2);//�����
			indices[2] = (source[2] << 6) + source[3];//�����

			//� �������� ������ ���������� ������� �� ���������� ��������
			for (int i = 0; i < 3; ++i)
				decodedStr[encodedStrIdx++] = indices[i];
		}
	}
	
	return decodedStr;
}
