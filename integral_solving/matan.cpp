#include<iostream>
#include<stdarg.h>
using namespace std;

double f1(double x)
{
	return x*x;
}

double f2(double x)
{
	return sin(x);
}

double sum(int count, int x, ...)
{
	va_list args;
	va_start(args, x);
	int res = x;
	for (int i = 1; i < count; i++) 
		res += va_arg(args, int);
	va_end(args);
	return res;
}

int mult(int x, ...)
{
	va_list args;
	va_start(args, x);
	int res = 1;

	while (x != 0.0) {
		res *= x;
		x = va_arg(args, int);
	}
	va_end(args);
	return res;
}


double rect_integral(double(*func)(double),double a, double b, int count)
{
	double dx = (b - a) / count;
	double res = 0;
	while (a < b) {
		res += func(a + dx / 2)*dx;
		a += dx;
	}
	return res;
}
double trap_integral(double(*func)(double), double a, double b, int count)
{
	double dx = (b - a) / count;
	double res = 0;
	while (a < b) {
		res += (func(a) + func(a+dx))*dx/2;
		a += dx;
	}
	return res;
}


int main() {
	double a, b;
	int count;
	cout << "enter params";
	cin >> a >> b >> count;
	cout << rect_integral(f1, a, b,count)<<"  ";
	cout << trap_integral(f1, a, b, count) << endl;
	//double(*func)(double) = f1;//�������. ���� (�������)
	cout << mult(5, 1, 2, 3, 0.0);
	system("pause");
	return 0;
}