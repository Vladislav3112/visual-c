#include "fraction.h"
#include<iostream>
#include"math.h"
fraction::fraction() {
	numerator = 0;
	denominator = 1;
}

fraction::fraction(int a, int b)
{
	if (b == 0) std::cout << "incorrect denominator!" << std::endl;
	numerator = a;
	denominator = b;
}

fraction& fraction::reduce()
{
	if (numerator != 0 && numerator!=0) {
		int n = nod(abs(denominator), abs(numerator));
		denominator /= n;
		numerator /=n;
	}
	return *this;
}

int fraction::nod(int n, int m)
{
	while (n != m) {
		if (n > m)n -= m;
		else m -= n;
	}
	return n;
}

fraction fraction::sum(fraction f)
{
	int n = nod(abs(denominator), abs(f.denominator));
	int o_denom = (denominator*f.denominator) / n;
	return fraction((numerator*o_denom)/ denominator + (f.numerator*o_denom)/ f.denominator,o_denom).reduce().check_sign();
}

fraction fraction::sub(fraction f)
{
	int n = nod(abs(denominator), abs(f.denominator));
	int o_denom = (denominator*f.denominator) / n;
	return fraction((numerator*o_denom) / denominator - (f.numerator*o_denom) / f.denominator, o_denom).reduce().check_sign();
}

fraction fraction::mult(fraction f)
{
	return fraction(numerator*f.numerator,denominator*f.denominator).reduce().check_sign();
}

fraction fraction::div(fraction f)
{ 
	if (f.numerator == 0) {
		std::cout << "divide by zero!" << std::endl;
		return fraction(0, 0);
	}return fraction(numerator*f.denominator, denominator*f.numerator).reduce().check_sign();
}

fraction& fraction::check_sign()
{
	if (denominator <= 0 && numerator <= 0) {
		denominator = -denominator;
		numerator = -numerator;
	}
	return *this;
}
