#include "math.h"
#include "complex.h"
#include <iostream>
using namespace std;
	complex::complex() {
		imagine = 0;
		real = 0;
		cout << "complex(double,double)" << endl;
		print();
	}
	complex::complex(double a, double b) {
		real= a;
		imagine = b;
		cout << "complex(double,double)" << endl;
		print();
	}
	complex::complex(const complex& source) {
		imagine = source.imagine;
		real = source.real;
		cout << "complex(const complex&)" << endl;
		print();
	}
	void complex::print() const {
		cout << "(" << real << "," << imagine << ")" << endl;
	}
	double complex::getReal()const {
		return real;
	}
	double complex::getImagine()const {
		return imagine;
	}
	complex complex::sum(complex c1) {
		return complex(c1.real + real, c1.imagine + imagine);
	}
	complex complex::sub(complex c1) {
		return complex(c1.real - real, c1.imagine - imagine);
	}
	complex complex::conj() { //сопряжение
		return complex(real,-imagine);
	}
	complex complex::mult(complex c1) {
		complex res;
		res.imagine = c1.imagine*real + c1.real*imagine;//bc+ad
		res.real = c1.real*real - c1.imagine*imagine;//ac-bd
		return res;
	}
	double complex::abs1() {
		return (sqrt(real*real + imagine*imagine));
	}
	double complex::arg() {
		return(asin(real /abs1()));
	}
	complex operator+(const complex& c1, const complex& c2)
	{
		return complex(c1.getReal()+c2.getReal(),c1.getImagine()+c2.getImagine());
	}
	complex& complex::operator=(const complex& c) {//useless
		real = c.real;
		imagine = c.imagine;
		cout << "assignment" << endl;
		return *this;
	}

	complex & complex::operator++()
	{
		++real;
		return *this;
	}

	complex complex::operator++(int)
	{
		complex tmp(real, imagine);
		++real;
		return tmp;
	}

	complex complex::testMethod()
	{
		/*complex tmp(1, 5);
		return tmp;*/
		complex* tmp = new complex(1, 5);
		return *tmp; //- 1 конструктор
	}

	complex& complex::testRefMethod()
	{
		complex tmp(1, 5);
		return tmp;
	}