#include<iostream>
#include<fstream>
using namespace std;

struct Point {
	int x;
	int y;
};
#pragma pack (push,1)
struct TestStruct {
	int x;
	char c;
};


#pragma pack(pop)
struct BitStruct {
	unsigned int x : 16; // ����������� � 32 ��� ������
	unsigned int y : 8;
	unsigned int z : 8;
};

union TestUnion {  //=struct, �� ����� � ������ ������ ������� ��������� ������ 1 ����������
	int x;
	double y;
	char z;
};

enum ParameterType { integer = 1, rational = 3, character };//{0,1}

struct Parameter {
	ParameterType type;//= 1, 3 ��� 4
	TestUnion value;
};

enum DayOfWeek {
	Monday = 0,
	Tuesday,
	Thursday,
	Wendsday,
	Friday,
	Saturday,
	Sunday
};
char daysConverter[7][16]{
	"Monday",
	"Tuesday",
	"Thursday",
	"Wendsday",
	"Friday",
	"Saturday",
	"Sunday"
};

void printDayOfWeek1(DayOfWeek day) {
	switch (day) {

	case Monday: {
		cout << "Today is Monday:" << endl;
		break;
	}
	case Tuesday: {
		cout << "Today is Tuesday:" << endl;
		break;
	}
	case Wendsday: {
		cout << "Today is Wendsday:" << endl;
		break;
	}
	case Thursday: {
		cout << "Today is Thursday:" << endl;
		break;
	}
	case Friday: {
		cout << "Today is Friday:" << endl;
		break;
	}
	case Saturday: {
		cout << "Today is Saturday:" << endl;
		break;
	}
	case Sunday: {
		cout << "Today is Sunday:" << endl;
		break;
	}
	}
}
	
void printDayOfWeek2(DayOfWeek day) {
	cout << "Today is " << daysConverter[day] << endl;
}

int main() {
	Point p1;
	p1.x = 10;
	p1.y = 20;

	cout << p1.x << " " << p1.y << endl;

	Point* p_dynamic = new Point;
	(*p_dynamic).x = 2; 
	p_dynamic->y = 25;// ~(*p_dynamic).y = 25

	cout << (*p_dynamic).x << " " << (*p_dynamic).y << endl;
	
	cout << "Size of Point:" << endl;
	cout << sizeof(Point) << endl;

	cout << "Size of TestStruct:" << endl;
	cout << sizeof(TestStruct) << endl;

	TestStruct outStruct;
	outStruct.x = 100;
	outStruct.c = 'a';

	ifstream in;
	in.open("out.dat", ios::binary);
	if (!in.is_open())
	{
		cout << "Out file error" << endl;
		system("pause");
		return -1;
	}
	in.read((char*)&outStruct, sizeof(TestStruct));//������� � ������� ����� � 
												   //���������������� outStruct ��� char*
	in.close();
	cout << outStruct.x << " " << outStruct.c << endl;

	cout <<"bitStruct size: "<< sizeof(BitStruct) << endl;

	BitStruct bS = { 1,2,3 };
	cout << bS.x << " " << bS.y << " " << bS.z << " " << endl;
	
	
	printDayOfWeek1(Monday);
	cout << "\n";
	printDayOfWeek2(Sunday);
	
	for (int i = 0; i < 7; i++) {
		printDayOfWeek2((DayOfWeek)i);
	}
	delete p_dynamic;
	system("pause");
	return 0;
}