#include <iostream>
#include "BmpStructures.h"
#include "ImgFunctions.h"
#include <fstream>
using namespace std;

int main()
{
	RgbImg sourceImg;
	ReadRgbImage("kidsnoise.bmp", sourceImg);

	//BlurImg(sourceImg, 3);
	MedianFilterImg(sourceImg, 1);
	RgbToGray(sourceImg);

	WriteRgbImg("gray.bmp", sourceImg);

	ClearImgMemory(sourceImg);
	system("pause");
	return 0;
}