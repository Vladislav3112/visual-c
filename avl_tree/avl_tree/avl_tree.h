using namespace std;
#pragma once
template <typename Type>
class AvlTree
{
private:
	enum Balance { d2left = -2, dleft, ok, dright, d2right };
	enum Side { left = 0, right };

	struct Node
	{
		Node* parent;
		Node* left;
		Node* right;
		Type data;
		Balance status;

		Node(Type _data) : left(nullptr),
			right(nullptr),
			parent(nullptr),
			status(Balance::ok),
			data(_data) {}
		Node* min() {
			Node* subroot = this;
			if (subroot == nullptr)return nullptr;
			while (subroot->left != nullptr)subroot = subroot->left;
			return subroot;
		}
		Node* max() {
			Node* subroot = this;
			while (subroot->right != nullptr)subroot = subroot->right;
			return subroot;
		}
		Node* next() {
			Node* subroot = this;
			if (subroot->right != nullptr)
				return subroot->right->min();

			Type currData = subroot->data;
			do 	subroot = subroot->parent;
			while (subroot != nullptr && subroot->data < currData);
			return subroot;
		}
	};
	Node* root;
	int size;
	int height;

	void addItemSubtree(const Type& item, Node* subroot);
	void printSubtree(Node* subroot) const;
	void deleteSubtree(Node* subroot);

	Node* rotate(Node* subroot, Side side);
	int getHeight(Node* subroot) const;
	void updateBalance(Node* subroot)
	{
		int heightRight = getHeight(subroot->right);
		int heightLeft = getHeight(subroot->left);
		subroot->status = (Balance)(heightRight - heightLeft);
	}
	void rebalance(Node* subroot);




public:
	AvlTree() : root(nullptr), size(0) {}
	~AvlTree()
	{
		if (!root) return;
		deleteSubtree(root);
	}
	int getSize() const { return size; }
	void insert(const Type& data);
	void print() const
	{
		if (!root) return;
		printSubtree(root);
	}
	class Iterator
	{
	private:
		Node* current;
	public:
		Iterator(Node* curr = nullptr) : current(curr) {}

		Type& operator*() const { return current->data; }
		bool operator==(const Iterator& right) { return current == right.current; }
		bool operator!=(const Iterator& right) { return current != right.current; }
		Iterator& operator++()
		{

			if (!current)throw "Iterator out of bounds";
			current = current->next();

			return *this;
		}

		Iterator operator++(int n) {
			Node* tmp = current;
			current = current->next();
			return Iterator(tmp);
		}


	};

	Iterator begin() const {
		return Iterator(root ? root->min() : nullptr);
	}

	Iterator end() const {
		return Iterator();
	}
	Iterator find(const Type &data) const;

};

template <typename Type>
void AvlTree<Type>::insert(const Type& data)
{
	if (!root)
	{
		root = new Node(data);
		++size;
		return;
	}
	addItemSubtree(data, root);
}

template<typename Type>
typename AvlTree<Type>::Iterator AvlTree<Type>::find(const Type& currData) const
{
	Node* curr = root;
	while (currData != curr->data) {
		if (currData > curr->data) curr = curr->right;
		else curr = curr->left;
	}return curr;

}

template <typename Type>
void AvlTree<Type>::printSubtree(Node* subroot) const
{
	if (subroot->left) printSubtree(subroot->left);
	cout << subroot->data << endl;
	if (subroot->right) printSubtree(subroot->right);
}

template <typename Type>
void AvlTree<Type>::deleteSubtree(Node* subroot)
{
	if (subroot->left) deleteSubtree(subroot->left);
	if (subroot->right) deleteSubtree(subroot->right);
	delete subroot;
}

template <typename Type>
int AvlTree<Type>::getHeight(Node* subroot) const
{
	if (subroot == nullptr) return -1;
	if (subroot->left == nullptr && subroot->right == nullptr)
		return 0;

	int heightRight;
	int heightLeft;

	if (subroot->left == nullptr)
		heightLeft = -1;
	else
		heightLeft = getHeight(subroot->left);

	if (subroot->right == nullptr)
		heightRight = -1;
	else
		heightRight = getHeight(subroot->right);

	return 1 + (heightLeft > heightRight ? heightLeft : heightRight);
}

template <typename Type>
typename AvlTree<Type>::Node* AvlTree<Type>::rotate(Node* subroot, Side side)
{
	Node* child;
	if (side == Side::right)
	{
		child = subroot->left;
		subroot->left = child->right;
		if (child->right != nullptr)
			child->right->parent = subroot;
		child->right = subroot;
	}
	else
	{
		child = subroot->right;
		subroot->right = child->left;
		if (child->left != nullptr)
			child->left->parent = subroot;
		child->left = subroot;
	}

	if (subroot->parent != nullptr)
	{
		if (subroot->parent->left == subroot)
			subroot->parent->left = child;
		else
			subroot->parent->right = child;
	}

	Node* tmp = subroot->parent;
	subroot->parent = child;
	child->parent = tmp;

	return child;
}

template <typename Type>
void AvlTree<Type>::addItemSubtree(const Type& item, Node* subroot)
{
	if (item > subroot->data)
	{
		if (subroot->right == nullptr)
		{
			subroot->right = new Node(item);
			subroot->right->parent = subroot;
			updateBalance(subroot);
			++size;
			return;
		}
		addItemSubtree(item, subroot->right);
	}
	else if (item < subroot->data)
	{
		if (subroot->left == nullptr)
		{
			subroot->left = new Node(item);
			subroot->left->parent = subroot;
			updateBalance(subroot);
			++size;
			return;
		}
		addItemSubtree(item, subroot->left);
	}
	else
	{
		throw "Item already exist";
	}

	updateBalance(subroot);
	rebalance(subroot);
}

template <typename Type>
void AvlTree<Type>::rebalance(Node* subroot)
{
	if (subroot->status == Balance::d2left)
	{
		if (subroot->left->status == Balance::dleft)
		{
			subroot = rotate(subroot, Side::right);
			updateBalance(subroot);
			updateBalance(subroot->right);
		}
		else
		{
			subroot->left = rotate(subroot->left, Side::left);
			subroot = rotate(subroot, Side::right);
			updateBalance(subroot);
			updateBalance(subroot->left);
			updateBalance(subroot->right);
		}
	}
	else if (subroot->status == Balance::d2right)
	{
		if (subroot->right->status == Balance::dright)
		{
			subroot = rotate(subroot, Side::left);
			updateBalance(subroot);
			updateBalance(subroot->left);
		}
		else
		{
			subroot->right = rotate(subroot->right, Side::right);
			subroot = rotate(subroot, Side::left);
			updateBalance(subroot);
			updateBalance(subroot->left);
			updateBalance(subroot->right);
		}
	}
	if (subroot->parent == nullptr)
		root = subroot;
}