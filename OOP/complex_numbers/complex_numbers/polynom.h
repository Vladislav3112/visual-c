#pragma once
#include "polynom.h"
#include <iostream>
class Polynom {
public:
	Polynom();
	Polynom(double* _coeff, int deg);
	Polynom(const Polynom& polynom);
	int getLenght() const;
	~Polynom();
	double& operator[](int index);
	double operator()(double x);
	void print() const;
private:
	double* coeffs;
	int length;
};


Polynom::Polynom()
{
	length = 1;
	coeffs = new double[1];
	coeffs[0] = 0;
}

int Polynom::getLenght() const
{
	return length;
}
Polynom::~Polynom()
{
	delete[] coeffs;
}

double & Polynom::operator[](int index)
{
	return coeffs[index];
}

double Polynom::operator()(double x)
{
	double res = 0;
	double currXdeg = 1;
	for (int i = 0; i < length; i++)
	{
		res += coeffs[i] * currXdeg;
		currXdeg *= x;
	}
	return res;
}

void Polynom::print() const
{
	for (int i = length - 1; i >= 0; i--)
		std::cout << coeffs[i] << "x^" << i << "+";
	std::cout << std::endl;
}
Polynom::Polynom(double* coeff, int len)
{
	length = len;
	coeffs = new double[length];
	for (int i = 0; i < length; i++)
		coeffs[i] = coeff[i];
}

Polynom::Polynom(const Polynom & polynom)
{
	length = polynom.length;
	coeffs = new double[length];
	for (int i = 0; i < length; i++)
		coeffs[i] = polynom.coeffs[i];
}